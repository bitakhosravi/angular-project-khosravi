import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Subject} from "rxjs";
import {Chat1} from "../parent-chat/chat1";




@Component({
  selector: 'app-chat1',
  templateUrl: './chat1.component.html',
  styleUrls: ['./chat1.component.css']
})
export class Chat1Component implements OnInit {

  constructor() { }

 //chat1:Chat1;
  chat1:Chat1 = {message : ''};

  @Input()
  chatArray2 : Subject<any[]>;

  Array2=[];

   @Output()
   saveMessage1:EventEmitter<any> = new EventEmitter();

  ngOnInit() {

    this.chatArray2.subscribe( r => {
      this.Array2 = r;
    });

  }
  save()
  {
    this.saveMessage1.emit(this.chat1);
  }



}
