import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatRoutingModule } from './chat-routing.module';
import {FormsModule} from "@angular/forms";
import { ParentChatComponent } from './parent-chat/parent-chat.component';
import { Chat1Component } from './chat1/chat1.component';
import { Chat2Component } from './chat2/chat2.component';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';


@NgModule({
  declarations: [ParentChatComponent, Chat1Component, Chat2Component],
  imports: [
    CommonModule,
    FormsModule,
    ChatRoutingModule,
    ButtonModule,
    TableModule,
  ]
})
export class ChatModule { }
