import {Component, OnInit} from '@angular/core';
import {Subject} from "rxjs";
import {ManageChat1Service} from "../../manage-chat1.service";
import {ManageChat2Service} from "../../manage-chat2.service";
import {Chat1} from "./chat1";
import {Chat2} from "./chat2";

@Component({
  selector: 'app-parent-chat',
  templateUrl: './parent-chat.component.html',
  styleUrls: ['./parent-chat.component.css']
})
export class ParentChatComponent implements OnInit {

  constructor(private ManageChat1:ManageChat1Service
  ,private ManageChat2:ManageChat2Service) { }

  chatArray1 : Subject<any[]> = new Subject<any[]>();
  chat1:Chat1 = {message : ''};
  chatArray2 : Subject<any[]> = new Subject<any[]>();
  chat2:Chat2  = {message : ''};



  ngOnInit() {
   this.get1();
   this.get2();
  }


  get1()
  {
  this.chatArray1.next(this.ManageChat1.getChat());
  if (this.chatArray1) {
    this.chat1 = this.chatArray1[0];
  }
  }
  save1($event)
  {
    this.ManageChat1.saveChat($event);
    this.get1();
  }
  get2()
  {
    this.chatArray2.next(this.ManageChat2.getChat());
    if (this.chatArray2) {
      this.chat2 = this.chatArray2[0];
    }
  }
  save2($event)
  {
    this.ManageChat2.saveChat($event);
    this.get2();
  }
}
