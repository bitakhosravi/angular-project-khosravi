import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ParentChatComponent} from "./parent-chat/parent-chat.component";



const routes: Routes = [
  {path: '', component : ParentChatComponent },
  {path: 'parent-chat', component :ParentChatComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ChatRoutingModule { }
