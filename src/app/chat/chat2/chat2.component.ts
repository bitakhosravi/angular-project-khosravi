import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Subject} from "rxjs";
import {Chat2} from "../parent-chat/chat2";



@Component({
  selector: 'app-chat2',
  templateUrl: './chat2.component.html',
  styleUrls: ['./chat2.component.css']
})
export class Chat2Component implements OnInit {

  constructor() { }


 // chat2:Chat2;
  chat2:Chat2 = {message : ''};

  @Input()
  chatArray1 : Subject<any[]>;

  Array1 =[];
  @Output()
  saveMessage2:EventEmitter<any> = new EventEmitter();


  ngOnInit() {

    this.chatArray1.subscribe( res => {
      this.Array1=res;
    });
    }

  save()
  {
    this.saveMessage2.emit(this.chat2);
  }


}
