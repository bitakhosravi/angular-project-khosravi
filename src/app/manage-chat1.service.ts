import { Injectable } from '@angular/core';
import {Chat1} from "./chat/parent-chat/chat1";



@Injectable({
  providedIn: 'root'
})
export class ManageChat1Service {

  constructor() { }

  saveChat(chat1 : Chat1 ) {
    let chatArray1 = [];
    try {
      chatArray1 = JSON.parse(localStorage.getItem('chat1'));
    } catch (e) {
      chatArray1 = [];
    }
    if (!chatArray1) {
      chatArray1 = [];
    }
    chatArray1.push(chat1);
    localStorage.setItem('chat1', JSON.stringify(chatArray1));
  }

  getChat(): Chat1[] {
    return JSON.parse(localStorage.getItem('chat1'));
  }

}
