import {RouterModule, Routes} from '@angular/router';
import { NgModule } from '@angular/core';


const r: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule)},
  ];

@NgModule({
  imports: [RouterModule.forRoot(r)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
