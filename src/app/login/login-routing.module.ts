import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from "./login/login.component";


const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  {path: 'employee', loadChildren: () => import('../employee/employee.module').then(m => m.EmployeeModule)},
  {path: 'chat', loadChildren: () => import('../chat/chat.module').then(m => m.ChatModule)},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
