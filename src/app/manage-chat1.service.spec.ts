import { TestBed } from '@angular/core/testing';

import { ManageChat1Service } from './manage-chat1.service';

describe('ManageChat1Service', () => {
  let service: ManageChat1Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ManageChat1Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
