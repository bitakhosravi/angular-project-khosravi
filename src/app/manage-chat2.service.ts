import { Injectable } from '@angular/core';
import {Chat2} from "./chat/parent-chat/chat2";

@Injectable({
  providedIn: 'root'
})
export class ManageChat2Service {

  constructor() { }
  saveChat(chat2 : Chat2 ) {
    let chatArray2 = [];
    try {
      chatArray2 = JSON.parse(localStorage.getItem('chat2'));
    } catch (e) {
      chatArray2 = [];
    }
    if (!chatArray2) {
      chatArray2 = [];
    }
    chatArray2.push(chat2);
    localStorage.setItem('chat2', JSON.stringify(chatArray2));
  }

  getChat(): Chat2[] {
    return JSON.parse(localStorage.getItem('chat2'));
  }

}
