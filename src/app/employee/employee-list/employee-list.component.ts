import { Component, OnInit } from '@angular/core';
import {RestClientService} from "../../rest-client.service";
import {Employee} from "./employee";

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  constructor(private restClient: RestClientService) { }

  employeeList = [];
  employee: Employee;


  ngOnInit() {

    this.employee = {id: null, name: '', salary: null, age: null };
    this.getAllEmployees();
  }

  getAllEmployees() {
    this.restClient.getEmployees().subscribe(r => {
      console.log('employee list', r);
      this.employeeList = r.data;
    });
  }

  deleteEmployee(id: number) {
    this.restClient.deleteEmployee(id).subscribe(r => {
      console.log('employee deleted');
    });
  }

}
