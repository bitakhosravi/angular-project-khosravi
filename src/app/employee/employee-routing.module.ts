import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EmployeeListComponent} from "./employee-list/employee-list.component";
import {RegisterEmployeeComponent} from "./register-employee/register-employee.component";


const routes: Routes = [
  {path: '', component : EmployeeListComponent },
  {path: 'employee-list', component : EmployeeListComponent },
  {path: 'register-employee', component : RegisterEmployeeComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
