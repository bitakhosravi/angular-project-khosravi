import { Component, OnInit } from '@angular/core';
import {RestClientService} from "../../rest-client.service";
import {Employee} from "../employee-list/employee";


@Component({
  selector: 'app-register-employee',
  templateUrl: './register-employee.component.html',
  styleUrls: ['./register-employee.component.css']
})
export class RegisterEmployeeComponent implements OnInit {

  constructor(private restClient: RestClientService) { }
  employeeList = [];
  employee: Employee;

  ngOnInit()  {
    this.employee = {id: null, name: '', salary: null, age: null };

  }

  saveEmployee() {
    this.restClient.saveEmployee(this.employee).subscribe(response => {
      console.log('server response', response)
    });
  }





}
