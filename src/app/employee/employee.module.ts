import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeRoutingModule } from './employee-routing.module';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { RegisterEmployeeComponent } from './register-employee/register-employee.component';
import {TableModule} from 'primeng/table';
import {ButtonModule} from 'primeng/button';
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [EmployeeListComponent, RegisterEmployeeComponent],
  imports: [
    CommonModule,
    FormsModule,
    EmployeeRoutingModule,
    TableModule,
    ButtonModule,
  ]
})
export class EmployeeModule { }
