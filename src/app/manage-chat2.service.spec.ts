import { TestBed } from '@angular/core/testing';

import { ManageChat2Service } from './manage-chat2.service';

describe('ManageChat2Service', () => {
  let service: ManageChat2Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ManageChat2Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
